Titanium.UI.setBackgroundColor('#000');
var tabGroup = Titanium.UI.createTabGroup();





var win1 = Titanium.UI.createWindow({  
    title:'CPU USAGE',
    backgroundColor:'#fff'
});
var tab1 = Titanium.UI.createTab({  
    icon:'KS_nav_views.png',
    title:'CPU',
    window:win1
});

var webview = Titanium.UI.createWebView({url:'/app.html'});
win1.add(webview);

var win2 = Titanium.UI.createWindow({  
    title:'RAM USAGE',
    backgroundColor:'#fff'
});
var tab2 = Titanium.UI.createTab({  
    icon:'KS_nav_ui.png',
    title:'RAM',
    window:win2
});

var ram = Titanium.UI.createWebView({url:'/ram.html'});
win2.add(ram);

////

var win3 = Titanium.UI.createWindow({  
    title:'DISK USAGE',
    backgroundColor:'#fff'
});
var tab3 = Titanium.UI.createTab({  
    icon:'KS_nav_ui.png',
    title:'DISK',
    window:win3
});

var disk = Titanium.UI.createWebView({url:'/disk.html'});
win3.add(disk);


var win4 = Titanium.UI.createWindow({  
    title:'Net',
    backgroundColor:'#fff'
});
var tab4 = Titanium.UI.createTab({
	icon:'KS_nav_ui.png',  
    title:'Net',
    window:win4
});

var net = Titanium.UI.createWebView({url:'/net.html'});
win4.add(net);
////

var win5 = Titanium.UI.createWindow({  
    title:'Process',
    backgroundColor:'#fff'
});
var tab5 = Titanium.UI.createTab({
	icon:'KS_nav_ui.png',  
    title:'Process',
    window:win5
});

var process = Titanium.UI.createWebView({url:'/process.html'});
win5.add(process);

///
tabGroup.addTab(tab1);  
tabGroup.addTab(tab2);
tabGroup.addTab(tab3);
tabGroup.addTab(tab4);
tabGroup.addTab(tab5);      

tabGroup.open();
